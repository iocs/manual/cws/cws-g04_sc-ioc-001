# Startup for CWS-G04:SC-IOC-001

# Load required modules
require essioc
require s7plc
require modbus
require calc

# Load standard IOC startup scripts
iocshLoad("$(essioc_DIR)/common_config.iocsh")

# Load PLC specific startup script
iocshLoad("$(E3_CMD_TOP)/iocsh/cws_g04_ctrl_plc_01.iocsh", "DBDIR=$(E3_CMD_TOP)/db/, MODVERSION=$(PLCIOCVERSION=$(IOCVERSION=$(DEFAULT_PLCIOCVERSION)))")

